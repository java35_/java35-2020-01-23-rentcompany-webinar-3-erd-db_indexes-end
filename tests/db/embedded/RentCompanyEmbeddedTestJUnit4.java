package db.embedded;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import db.entities.CarEntity;
import db.entities.State;

public class RentCompanyEmbeddedTestJUnit4 {
	IRentCompanyEmbedded rentCompanyEmbedded;
	
	static List<CarEntity> cars;
	
	final CarEntity NO_EXISTS_CAR = new CarEntity("a700", "White", State.EXCELENT, 
											"Kia Rio 1", true, false);
	@BeforeClass
	public static void beforeClass() {
		cars = getCars();
	}
	
	@Before
	public void setUp() {
		rentCompanyEmbedded = new RentCompanyEmbedded();
		for (CarEntity car : cars) {
			rentCompanyEmbedded.saveCar(car);
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void saveCar() {
		CarEntity actualCar = rentCompanyEmbedded.saveCar(NO_EXISTS_CAR);
		assertEquals(NO_EXISTS_CAR, actualCar);
		rentCompanyEmbedded.saveCar(null);
	}
	
	private static List<CarEntity> getCars() {
		CarEntity car100 = new CarEntity("a100", "White", State.EXCELENT, 
				"Kia Rio 1", true, false);
		CarEntity car200 = new CarEntity("a200", "Black", State.GOOD, 
				"Kia Rio 1", false, false);
		CarEntity car300 = new CarEntity("a300", "Grey", State.EXCELENT, 
				"Kia Rio 1", false, false);
		CarEntity car400 = new CarEntity("a400", "White", State.EXCELENT, 
				"Chevrolet cruze 1", true, false);
		CarEntity car500 = new CarEntity("a500", "Black", State.BAD, 
				"Chevrolet cruze 1", false, true);
		CarEntity car600 = new CarEntity("a600", "Grey", State.EXCELENT, 
				"Chevrolet cruze 1", true, false);
		List<CarEntity> cars = new ArrayList<>();
		cars.add(car100);
		cars.add(car200);
		cars.add(car300);
		cars.add(car400);
		cars.add(car500);
		cars.add(car600);
//		List<CarEntity> cars = List.of(car100, car200, car300, car400, car500, car600);
//		return cars;
		return cars;
	}
}
