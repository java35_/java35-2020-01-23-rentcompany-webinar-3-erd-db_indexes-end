package service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import db.CarsRepository;
import db.DriversRepository;
import db.IRepository;
import db.ModelsRepository;
import db.embedded.ModelNotExistsException;
import db.entities.CarEntity;
import db.entities.DriverEntity;
import db.entities.ModelEntity;
import db.entities.State;
import ui.CarsReturnCode;
import ui.DriversState;
import ui.ModelState;
import ui.dto.CarDto;

public class RentCompanyService {
	IRepository<DriverEntity, 	Integer> 	driverRepository 	= new DriversRepository();
	IRepository<ModelEntity, 	String> 	modelRepository 	= new ModelsRepository();
	CarsRepository 							carsRepository 		= new CarsRepository();

	public DriversState addDriver(int licenseId, String name, LocalDate birthDate, String phone) {
		if (driverRepository.existsById(licenseId))
			return DriversState.ALL_READY_EXISTS;
		DriverEntity driverEntity = new DriverEntity(licenseId, name, birthDate, phone);
		driverEntity = driverRepository.save(driverEntity);
		if (driverEntity == null)
			return DriversState.ERROR;
		return DriversState.OK;
	}

	public ModelState addModel(String modelName, int gasTank, String company, String country, int priceDay) {
		if (modelRepository.existsById(modelName))
			return ModelState.ALL_READY_EXISTS;
		ModelEntity modelEntity = new ModelEntity(modelName, gasTank, company, country, priceDay);
		modelEntity = modelRepository.save(modelEntity);
		if (modelEntity == null)
			return ModelState.ERROR;
		return ModelState.OK;
	}

	public CarsReturnCode addCar(String regNumber, String color, State state, String modelName) {
		if (carsRepository.existsById(regNumber))
			return CarsReturnCode.ALL_READY_EXISTS;
		CarEntity carEntity = new CarEntity(regNumber, color, state, modelName, false, false);
		CarEntity carEntityResult = null;
		try {
			carEntityResult = carsRepository.save(carEntity);
		} catch (ModelNotExistsException e) {
			return CarsReturnCode.MODEL_NOT_EXISTS;
		}
		return carEntityResult == null ? CarsReturnCode.ERROR : CarsReturnCode.OK;
	}

	public List<CarDto> findCarsByModel(String modelName) {
		if (modelName == null || modelName.isBlank())
			return List.of();
		return carsRepository.findCarsByModel(modelName).stream()
				.map(cEntity -> cnvCarFromEntityToDto(cEntity))
				.collect(Collectors.toList());
	}
	
//	public DriverDto getDriverById(int id) {
//		// driverEntity -> DriverDto
//	}
	
	private CarDto cnvCarFromEntityToDto(CarEntity entity) {
		assert entity != null;
		return new CarDto(entity.getRegNumber(), 
							entity.getColor(), 
							entity.getState(), 
							entity.getModelName(), 
							entity.isInUse());
	}
}
