package db.embedded;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import db.entities.CarEntity;
import db.entities.DriverEntity;
import db.entities.ModelEntity;
import ui.dto.CarDto;

public class RentCompanyEmbedded implements IRentCompanyEmbedded {
	private Map<String, CarEntity> 		cars 	= new HashMap<>();
	private Map<Integer, DriverEntity> 	drivers = new HashMap<>();
	private Map<String, ModelEntity> 	models 	= new HashMap<>();
	
	private Map<String, List<CarEntity>> carsIndexByModel = new HashMap<>();
	
	// Cars *****************************************************
	@Override
	public CarEntity saveCar(CarEntity entity) {
		if (entity == null)
			throw new IllegalArgumentException();
		if (!models.containsKey(entity.getModelName()))
			throw new ModelNotExistsException();
		CarEntity carEntity = new CarEntity(entity);
		cars.put(carEntity.getRegNumber(), carEntity);
		
		List<CarEntity> carsByModelName = carsIndexByModel.get(carEntity.getModelName());
		if (carsByModelName == null) {
			carsByModelName = new ArrayList<>();
			carsByModelName.add(carEntity);
			carsIndexByModel.put(carEntity.getModelName(), carsByModelName);
		} else {
			carsByModelName.add(carEntity);
		}
		
		return new CarEntity(cars.get(entity.getRegNumber()));
	}
	
	public boolean existsCarById(String id) {
		if (id == null || id.isBlank())
			throw new IllegalArgumentException();
		return cars.containsKey(id);
		
	}
	
	public List<CarEntity> findCarsByModel(String modelName) {
		return carsIndexByModel.getOrDefault(modelName, new ArrayList<>()).stream()
				.filter(c -> !c.isFlRemoved())
				.collect(Collectors.toList());
	}
	// Cars *****************************************************
	
	// Models *****************************************************
	public ModelEntity saveModel(ModelEntity entity) {
		if (entity == null)
			throw new IllegalArgumentException();
		ModelEntity modelEntity = new ModelEntity(entity);
		models.put(modelEntity.getModelName(), modelEntity);
//		return new DriverEntity(drivers.get(driverEntity.getLicenseId()));
		return new ModelEntity(modelEntity);
	}
	
	public ModelEntity findModelById(String id) {
		if (id == null || id.isBlank())
			throw new IllegalArgumentException();
		return new ModelEntity(models.get(id));
	}

	public List<ModelEntity> findAllModels() {
		return models.values().stream()
				.map(m -> new ModelEntity(m))
				.collect(Collectors.toList());
	}

	public int countModels() {
		return models.size();
	}

	public void deleteModel(String id) {
		if (id == null || id.isBlank())
			throw new IllegalArgumentException();
		models.remove(id);
	}

	public boolean existsByModelId(String id) {
		if (id == null || id.isBlank())
			throw new IllegalArgumentException();
		return models.containsKey(id);
	}
	// Models *****************************************************
	
	// Drivers *****************************************************
	public DriverEntity saveDriver(DriverEntity entity) {
		if (entity == null)
			throw new IllegalArgumentException();
		DriverEntity driverEntity = new DriverEntity(entity);
		drivers.put(driverEntity.getLicenseId(), driverEntity);
//		return new DriverEntity(drivers.get(driverEntity.getLicenseId()));
		return new DriverEntity(driverEntity);
	}
	
	public DriverEntity findDriverById(Integer id) {
		if (id <= 0)
			throw new IllegalArgumentException();
		return new DriverEntity(drivers.get(id));
	}

	public List<DriverEntity> findAllDrivers() {
		return drivers.values().stream()
				.map(d -> new DriverEntity(d))
				.collect(Collectors.toList());
	}

	public int countDrivers() {
		return drivers.size();
	}

	public void deleteDriver(Integer id) {
		if (id <= 0)
			throw new IllegalArgumentException();
		drivers.remove(id);
	}

	public boolean existsByDriverId(Integer id) {
		if (id <= 0)
			throw new IllegalArgumentException();
		return drivers.containsKey(id);
	}
	// Drivers *****************************************************


}
