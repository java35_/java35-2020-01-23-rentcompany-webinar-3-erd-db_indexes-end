package db;

import java.util.List;

import db.entities.CarEntity;
import ui.dto.CarDto;

public class CarsRepository implements IRepository<CarEntity, String> {

	// CRUD - create, read, update, delete
	
	@Override
	public CarEntity findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CarEntity save(CarEntity entity) {
		if (entity == null)
			return null;
		return rentCompany.saveCar(entity);
	}

	@Override
	public List<CarEntity> findAll() {
		return null;
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean existsById(String id) {
		return rentCompany.existsCarById(id);
	}
	
	public List<CarEntity> findCarsByModel(String modelName) {
		if (modelName == null || modelName.isBlank())
			return List.of();
		return rentCompany.findCarsByModel(modelName);
	}

}
