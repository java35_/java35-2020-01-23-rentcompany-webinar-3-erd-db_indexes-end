package ui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import db.entities.State;
import service.RentCompanyService;
import ui.dto.CarDto;

public class RentCompanyController {

	static BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
	static RentCompanyService service = new RentCompanyService();
	
	public static void main(String[] args) throws Exception {
		System.out.println("�������� ����� ����:");
		System.out.println("1 - add driver");
		System.out.println("2 - add model");
		System.out.println("3 - add car");
		System.out.println("4 - findCarsByModel");
		System.out.println("5 - exit");
		
		int userAnswer = Integer.parseInt(scanner.readLine());
		
		while (userAnswer != 5) {
			switch (userAnswer) {
			case 1:
				addDriver();
				break;
			case 2:
				addModel();
				break;
			case 3:
				addCar();
				break;
			case 4:
				findCarsByModel();
				break;

			default:
				break;
			}
			System.out.println("�������� ����� ����:");
			System.out.println("1 - add driver");
			System.out.println("2 - add model");
			System.out.println("3 - add car");
			System.out.println("4 - findCarsByModel");
			System.out.println("5 - exit");
			
			userAnswer = Integer.parseInt(scanner.readLine());
		}
		scanner.close();
	}

	private static void findCarsByModel() throws Exception {
		System.out.println("Input modelName");
		String modelName = scanner.readLine();
		
		List<CarDto> cars = service.findCarsByModel(modelName);
		
		if (cars == null || cars.isEmpty()) {
			System.out.println("No cars by model name");
		} else
			cars.forEach(System.out::println);
	}

	private static void addCar() throws Exception {
		System.out.println("Input regNumber");
		String regNumber = scanner.readLine();
		System.out.println("Input color");
		String color = scanner.readLine();
		System.out.println("Input color");
		State state = State.valueOf(scanner.readLine());
		System.out.println("Input modelName");
		String modelName = scanner.readLine();
		
		CarsReturnCode returnedCode = service.addCar(regNumber, color, state, modelName);
		if (CarsReturnCode.OK == returnedCode) {
			System.out.println("Car added to db");
		} else if (CarsReturnCode.MODEL_NOT_EXISTS == returnedCode) {
			System.out.println("Model not exists. Please input model before.");
		} else {
			System.out.println("Car allready exists");
		}
	}

	private static void addDriver() throws Exception {
		System.out.println("Input licenseId");
		Integer licenseId = Integer.parseInt(scanner.readLine());
		System.out.println("Input name");
		String name = scanner.readLine();
		System.out.println("Input birthYear");
		LocalDate birthDate = LocalDate.parse(scanner.readLine());
		System.out.println("Input phone");
		String phone = scanner.readLine();
		
		if (DriversState.OK == service.addDriver(licenseId, name, birthDate, phone)) {
			System.out.println("Driver added to db");
		} else {
			System.out.println("Driver allready exists");
		}
	}
	
	private static void addModel() throws Exception {
		System.out.println("Input modelName");
		String modelName = scanner.readLine();
		System.out.println("Input gasTank");
		int gasTank = Integer.parseInt(scanner.readLine());
		System.out.println("Input company");
		String company = scanner.readLine();
		System.out.println("Input country");
		String country = scanner.readLine();
		System.out.println("Input priceDay");
		int priceDay = Integer.parseInt(scanner.readLine());
		
		if (ModelState.OK == service.addModel(modelName, gasTank, company, country, priceDay)) {
			System.out.println("Model added to db");
		} else {
			System.out.println("Model allready exists");
		}
	}

}
