package ui.dto;

import db.entities.State;

public class CarDto {
	private String regNumber;
	private String color;
	private State state;
	private String modelName;
	private boolean inUse;
	@Override
	public String toString() {
		return "CarsDto [regNumber=" + regNumber + ", color=" + color + ", state=" + state + ", modelName=" + modelName
				+ ", inUse=" + inUse + "]";
	}

	public CarDto(String regNumber, String color, State state, String modelName, boolean inUse) {
		this.regNumber = regNumber;
		this.color = color;
		this.state = state;
		this.modelName = modelName;
		this.inUse = inUse;
	}

	public String getRegNumber() {
		return regNumber;
	}
	public String getColor() {
		return color;
	}
	public State getState() {
		return state;
	}
	public String getModelName() {
		return modelName;
	}
	public boolean isInUse() {
		return inUse;
	}
	
	
}
