package ui;

public enum CarsReturnCode {
	OK,
	ALL_READY_EXISTS,
	MODEL_NOT_EXISTS,
	ERROR
}
